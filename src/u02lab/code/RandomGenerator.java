package u02lab.code;

import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private static final double HALF_INDEX = 0.50;

    private int sequenceLength;
    private int actualLength;

    public RandomGenerator(int sequenceLength){
        this.sequenceLength = sequenceLength;
        this.actualLength = 0;
    }

    @Override
    public Optional<Integer> next() {

        if(this.actualLength < this.sequenceLength){
            this.actualLength++;
            return Optional.of(Math.random() >= HALF_INDEX ? 1 : 0);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.actualLength = 0;
    }

    @Override
    public boolean isOver() {
        return this.actualLength >= this.sequenceLength;
    }

    @Override
    public List<Integer> allRemaining() {
        return null;
    }
}
