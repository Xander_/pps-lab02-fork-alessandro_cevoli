package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.time.Year;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 *
 * Created by Xander_C on 07/03/2017.
 */
public class RandomGeneratorTest {

    private static final int ATTEMPTS = 8;
    private static Integer GENERATION_LIMIT = 16;

    private SequenceGenerator randomGenerator;

    @Before
    void init(){
        this.randomGenerator = new RandomGenerator(GENERATION_LIMIT);
    }

    @Test
    public void assertNextAndResetCorrectness() throws Exception {

        for (int i = 0; i < GENERATION_LIMIT; i++){
            int next = randomGenerator.next().get();
            assertTrue(next == 0 || next == 1);
        }

        this.assertResetCorrectness();
    }

    private void assertResetCorrectness() {
        assertFalse(randomGenerator.next().isPresent());
        randomGenerator.reset();
        assertTrue(randomGenerator.next().isPresent());
        randomGenerator.reset();
    }

    @Test
    public void assertIsOverCorrectness(){
        for (int i = 0; i < GENERATION_LIMIT; i++){
            assertFalse(randomGenerator.isOver());
            randomGenerator.next().get();
        }

        assertTrue(randomGenerator.isOver());
    }

    @Test
    public void assertAllRemainsCorrectness(){

        for (int i = 0; i < ATTEMPTS; i++){
            randomGenerator.next().get();
        }

        for (int i = ATTEMPTS; i < GENERATION_LIMIT; i++){
            int next = randomGenerator.next().get();
            assertTrue(next == 0 || next == 1);
        }

        assertTrue(randomGenerator.isOver());
    }

}