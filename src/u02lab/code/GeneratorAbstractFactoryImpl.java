package u02lab.code;

/**
 *
 * Created by Xander_C on 07/03/2017.
 */
public class GeneratorAbstractFactoryImpl implements GeneratorAbstractFactory {

    public GeneratorAbstractFactoryImpl(){
        // NOPE
    }

    @Override
    public SequenceGenerator getNewRangeGenerator(int start, int stop) {
        return new RangeGenerator(start, stop);
    }

    @Override
    public SequenceGenerator getNewRandomGenerator(int sequenceLength) {
        return new RandomGenerator(sequenceLength);
    }

}
