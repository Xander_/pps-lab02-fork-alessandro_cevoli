package u02lab.code;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * Created by Xander_C on 07/03/2017.
 */
class RangeGeneratorTest {

    private static Integer START_NUMBER = 0;

    private static Integer END_NUMBER = 10;

    private static Integer ATTEMPTS = 5;

    private SequenceGenerator rangeGenerator;

    @Before
    void init(){
        this.rangeGenerator = new RangeGenerator(START_NUMBER, END_NUMBER);
    }

    @Test
    void assertNextAndResetCorrectness() {

        for (int next = START_NUMBER; next < END_NUMBER; next++){

            assertTrue(rangeGenerator.next().get() == next + 1);
        }

        this.assertResetCorrectness();

        rangeGenerator.reset();
    }


    private void assertResetCorrectness(){
        rangeGenerator.reset();
        assertTrue(rangeGenerator.next().get() == START_NUMBER + 1);
    }


    @Test
    void assertIsOverCorrectness(){

        for (int next = START_NUMBER; next < END_NUMBER; next++){
            assertFalse(rangeGenerator.isOver());
            rangeGenerator.next();
        }

        this.assertIsOverTrueCorrectness();
        rangeGenerator.reset();
    }


    private void assertIsOverTrueCorrectness(){

        assertTrue(rangeGenerator.isOver());
    }

    @Test
    void assertAllRemainingCorrectness(){
        for (int next = START_NUMBER; next < ATTEMPTS; next++){
            rangeGenerator.next();
        }

        int counter = ATTEMPTS;

        for (Integer next : rangeGenerator.allRemaining()){

            assertTrue(next == ++counter);
        }
    }
}