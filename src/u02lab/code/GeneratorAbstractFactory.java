package u02lab.code;

/**
 *
 * Created by Xander_C on 07/03/2017.
 */
public interface GeneratorAbstractFactory {

    SequenceGenerator getNewRangeGenerator(final int start, final int stop);

    SequenceGenerator getNewRandomGenerator(final int sequenceLength);
}
